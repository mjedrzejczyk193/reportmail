﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;

namespace ReportMail.Class
{
    class DbManager
    {
        SqlConnection connection;
        SqlCommand command;

        public DbManager()
        {
            connection = new SqlConnection();
            connection.ConnectionString = Properties.Settings.Default.pob_iNTRANETConnectionString;
            command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.Text;
        } // constructor

        public bool GetZakupData(ref string nazwa, ref string nip, ref string dokument, ref string data_platnosci, ref string kwota, ref string zaplacono, ref string saldo, ref string po_terminie, int idkontahenta, dok_zak_salda_na_dzienResult row, string stan_na_dzien, bool czyZPrzedTerminem)
        {
            bool returnvalue = false;
            DataClasses1DataContext db = new DataClasses1DataContext();
            db.CommandTimeout = 1 * 60 * 30;
            DateTime date1 = (DateTime)row.data_platnosci;

            DateTime date2 = DateTime.Parse(stan_na_dzien);
            TimeSpan result = date2 - date1;
            //Zapis dp stringa ilości dni jakie uplynely między datami
            string ResultDays = result.TotalDays.ToString();

            decimal wynik = Convert.ToDecimal(ResultDays);
            var pozostalo = row.saldo;
            dokument = row.dok.ToString();
            data_platnosci = date1.ToString("dd-MM-yyyy");
            kwota = Math.Round(row.kwota, 2).ToString();
            zaplacono = Math.Round(row.zaplacono, 2).ToString();
            saldo = pozostalo.ToString();
            var kontrahent = from kon in db.kontrahents where kon.idkontahenta == row.idkontahenta select kon;
            nazwa = kontrahent.First().nazwa;
            nip = kontrahent.First().nip;

            if (wynik != 0 && pozostalo != 0)
            {
                if (wynik <= 0)
                {
                    po_terminie = "";
                    if (!czyZPrzedTerminem)
                    {
                        Debug.WriteLine(string.Format("NIE :( dok: {0}, kon: {1}, data: {2}, kwota: {3}, zaplacono: {4}, saldo: {5}", dokument, nazwa, data_platnosci, kwota, zaplacono, saldo));
                        return false;
                    }
                }
                else
                {
                    po_terminie = Math.Floor(wynik).ToString() + " dni";
                    if (Math.Floor(wynik).ToString() == "1")
                    {
                        po_terminie = Math.Floor(wynik).ToString() + " dzień";
                    }
                    Debug.WriteLine(string.Format("TAK :) dok: {0}, kon: {1}, data: {2}, kwota: {3}, zaplacono: {4}, saldo: {5}", dokument, nazwa, data_platnosci, kwota, zaplacono, saldo));
                }
                returnvalue = true;
            }
            return returnvalue;
        }

        public List<int> getKontrahenci(string nazwaKontrahenta, DataClasses1DataContext db1, bool zIntranetu)
        {
            db1.CommandTimeout = 1 * 60 * 30;
            if (!zIntranetu)
            {
                List<int> listaKontrahentId = new List<int>();
                if (!nazwaKontrahenta.Contains("WSZYSCY"))
                {
                    var salda = from kon in db1.salda_zs where kon.saldo != 0 && kon.nazwa.Contains(nazwaKontrahenta) orderby kon.saldo select kon;
                    foreach (var s in salda)
                    {
                        //MessageBox.Show("Czyta 1 kontrahenta");
                        var kontrahent = from kon2 in db1.kontrahents where kon2.idkontahenta == Convert.ToInt32(s.idkontahenta) select kon2;
                        if (kontrahent.Any())
                        {
                            listaKontrahentId.Add(Convert.ToInt32(s.idkontahenta));
                        }
                    }
                }
                else
                {
                    var salda = from kon in db1.salda_zs where kon.saldo != 0 orderby kon.saldo select kon;
                    foreach (var s in salda)
                    {
                        //MessageBox.Show("Czyta wszystkich kontrahentów");
                        var kontrahent = from kon2 in db1.kontrahents where kon2.idkontahenta == Convert.ToInt32(s.idkontahenta) select kon2;
                        if (kontrahent.Any())
                        {
                            listaKontrahentId.Add(Convert.ToInt32(s.idkontahenta));
                        }
                    }
                }
                return listaKontrahentId;
            }
            else
            {
                List<int> listaKontrahentId = new List<int>();
                if (!nazwaKontrahenta.Contains("WSZYSCY"))
                {
                    var salda = from kon in db1.salda_zs where kon.saldo != 0 && kon.nazwa.Contains(nazwaKontrahenta) orderby kon.nazwa select kon;
                    foreach (var s in salda)
                    {
                        //MessageBox.Show("Czyta 1 kontrahenta");
                        var kontrahent = from kon2 in db1.kontrahents where kon2.idkontahenta == Convert.ToInt32(s.idkontahenta) && kon2.raporty == "1" select kon2;
                        if (kontrahent.Any())
                        {
                            listaKontrahentId.Add(Convert.ToInt32(s.idkontahenta));
                        }
                    }
                }
                else
                {
                    var salda = from kon in db1.salda_zs where kon.saldo != 0 orderby kon.nazwa select kon;
                    foreach (var s in salda)
                    {
                        //MessageBox.Show("Czyta wszystkich kontrahentów");
                        var kontrahent = from kon2 in db1.kontrahents where kon2.idkontahenta == Convert.ToInt32(s.idkontahenta) && kon2.raporty == "1" select kon2;
                        if (kontrahent.Any())
                        {
                            listaKontrahentId.Add(Convert.ToInt32(s.idkontahenta));
                        }
                    }
                }
                return listaKontrahentId;
            }
        }
    }
}

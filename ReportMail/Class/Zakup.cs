﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportMail.Class
{
    class Zakup
    {
        public double kwota;
        public DateTime data;
        public string nazwaKontrahenta;
        public int idkontrahenta;
        public string nip;

        public Zakup(double kw, DateTime dt, string nazwaK, int idk, string n)
        {
            kwota = kw;
            data = dt;
            nazwaKontrahenta = nazwaK;
            idkontrahenta = idk;
            nip = n;
        }
    }
}

﻿using System;
using System.Threading;
using System.Windows.Forms;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using System.Net.Mail;
using ReportMail.Class;
using System.Data;
using System.Linq;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Diagnostics;

namespace ReportMail
{
    public partial class Form1 : Form
    {
        static string path = @"C:\raporty\Raporty kontrahenci.xlsx";
        static string testPath = @"C:\raporty\test2.xlsx";

        static string path2 = @"C:\raporty\Raporty konta bankowe.xlsx";
        static string testPath2 = @"C:\raporty\test3.xlsx";

        static bool active = false;
        static bool test = false;

        static string zakupWorksheetName = "Szczegółowy - po terminie";
        static string zakupSumy2WorksheetName = "Dla Ewy";
        //static string zakupSumyWorksheetName = "Dla Rafała";
        static string rozliczenieKontBankowychBiezacyMiesiacName = string.Format("Konta bankowe-{0}", DataSlownie(DateTime.Now.Month, DateTime.Now.Year));
        static string rozliczenieKontBankowychOstatnieTrzyMiesiaceName = "Konta bankowe - 3 miesiące";
        static string rozliczenieKontBankowychOstatnieSześćMiesiacyName = "Konta bankowe - 6 miesiący";
        static string rozliczenieKontBankowychOstatniRokName = "Konta bankowe - 12 miesięcy";

        static FileInfo excelFile;
        static FileInfo excelFile2;
        static string stan_na_dzien;

        public Form1()
        {
            this.ShowInTaskbar = false;
            InitializeComponent();
            WypelnijComboKontrahent();

            textBox1.Enabled = false;
            button1.Visible = false;
            button1.Enabled = false;
            label1.Visible = false;

            dateTimePicker1.Value = DateTime.Now;
            stan_na_dzien = DateTime.Now.ToShortDateString();
        }

        private void Form1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                foreach (Process proc in Process.GetProcessesByName("ReportMail"))
                {
                    proc.Kill();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Thread proces = new Thread(new ThreadStart(PetlaNieskonczona));
            dateTimePicker1.Value = DateTime.Now;
            if (button1.Text == "Start")
            {
                button1.Text = "Stop";
                MessageBox.Show("Automatyczne raporty zostały włączone.");
                active = true;
                proces.Start();
            }
            else
            {
                button1.Text = "Start";
                MessageBox.Show("Automatyczne raporty zostały wyłączone.");
                active = false;
                proces.Abort();
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            if (chk.Checked)
            {
                textBox1.Enabled = true;
            }
            else
            {
                textBox1.Enabled = false;
                textBox1.Text = "ecembaluk@poburski.pl";
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            TextBox txb = (TextBox)sender;
            if (txb.Text.Contains("jczyk@wiz"))
            {
                button1.Visible = true;
                button1.Enabled = true;
            }
            else
            {
                button1.Visible = false;
                button1.Enabled = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            stan_na_dzien = dateTimePicker1.Value.ToShortDateString();
            label1.Text = "Czekaj...";
            label1.Visible = true;
            RaportSaldoKontrahentZakup(false, comboBox1.Text);
            SendRaport1(textBox1.Text);
            label1.Text = "Wysłano pomyślnie.";
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://int.poburski.local/aplikacjeASPX/05/005/RaportyKontrahenci.aspx");
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cbb = (ComboBox)sender;
            if (cbb.Text == "WSZYSCY (zgodnie z iNTRANET)")
            {
                linkLabel1.Visible = true;
            }
            else
            {
                linkLabel1.Visible = false;
            }
        }

        //działanie - metody

        private static void PetlaNieskonczona()
        {
            string wyslano = "";
            if (test)
            {
                while (true)
                {
                    RaportSaldoKontrahentZakup(false, "WSZYSCY"); //tworzy raport
                    //MessageBox.Show("Stworzyło raport");
                    SendRaport1("mjedrzejczyk@wizualizacja.com"); //wysyła raport testowy
                    //MessageBox.Show("Wysłano raport");
                    Thread.Sleep(1000 * 60 * 3); // 30 sekund
                    //Thread.Sleep(ms * ss * mm)
                }
            }
            else
            {
                while (true)
                {
                    if (active)
                    {
                        DateTime now = DateTime.Now;
                        if (now.ToShortDateString() != wyslano)
                        {
                            if (now.DayOfWeek == DayOfWeek.Wednesday || now.DayOfWeek == DayOfWeek.Friday)
                            {
                                if (now.Hour == 8)
                                {
                                    stan_na_dzien = DateTime.Now.ToShortDateString();
                                    RaportSaldoKontrahentZakup(false, "WSZYSCY");
                                    SendRaport1("mjedrzejczyk@wizualizacja.com");
                                    SendRaport1("rwojcik@poburski.pl");
                                    SendRaport1("awdowiak@poburski.pl");
                                    SendRaport1("ecembaluk@poburski.pl");
                                    wyslano = DateTime.Now.ToShortDateString();
                                    excelFile.Delete();
                                }
                            }
                        }
                        Thread.Sleep(1000 * 60 * Properties.Settings.Default.SendEveryMinute); // 30 Minutes
                    }
                }
            }
        }

        private static void SendRaport1(string mail)
        {
            string To = mail;
            string From = Properties.Settings.Default.From;
            string Subject = "Raport - salda kontrahentów na dzień " + stan_na_dzien;
            string Body = "<html><body><p>W załączniku raport z sald kontrahentów na dzień " + stan_na_dzien + ".</p><p>Do zmiany kontrahentów zaznaczyć/odznaczyć ich w intranecie pod adresem: " + "http://int.poburski.local/aplikacjeASPX/05/005/RaportyKontrahenci.aspx" + "</p></body><html>";

            MailMessage msg = new MailMessage(From, To, Subject, Body); // create the email message
            msg.IsBodyHtml = true;
            try // attachments
            {
                msg.Attachments.Add(new Attachment(path));
            }
            catch (Exception ex2)
            {
                Console.WriteLine(ex2);
                msg.Attachments.Add(new Attachment(testPath));
            }

            SmtpClient client = new SmtpClient();
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(Properties.Settings.Default.networkCredentialLogin, Properties.Settings.Default.networkCredentialPassword);
            client.Port = 587; // You can use Port 25 if 587 is blocked (mine is!)
            client.Host = Properties.Settings.Default.Server;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            try
            {
                client.Send(msg);
                Debug.WriteLine(string.Format("Wiadomość została wysłana do {0}", mail));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(string.Format("Wiadomość NIE została wysłana do {0}", mail));
                MessageBox.Show("Od: " + From + Environment.NewLine + "Do: " + To + Environment.NewLine + ex.Message);
                throw;
            }

            SendRaport2(To, From, client, mail);

            client.Dispose();
            msg.Dispose();
        }

        private static void SendRaport2(string To, string From, SmtpClient client, string mail)
        {
            string Subject = "Raport - salda kont bankowych na dzień " + stan_na_dzien;
            string Body = "<html><body><p>W załączniku raport z sald kont bankowych na dzień " + stan_na_dzien + ".</p></body><html>";

            MailMessage msg = new MailMessage(From, To, Subject, Body); // create the email message
            msg.IsBodyHtml = true;
            try // attachments
            {
                msg.Attachments.Add(new Attachment(path2));
            }
            catch (Exception ex2)
            {
                Console.WriteLine(ex2);
                msg.Attachments.Add(new Attachment(testPath2));
            }

            try
            {
                client.Send(msg);
                Debug.WriteLine(string.Format("Wiadomość została wysłana do {0}", mail));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(string.Format("Wiadomość NIE została wysłana do {0}", mail));
                MessageBox.Show("Od: " + From + Environment.NewLine + "Do: " + To + Environment.NewLine + ex.Message);
                throw;
            }
            msg.Dispose();
        }

        private static void RaportSaldoKontrahentZakup(bool testowy)
        {
            double sumaWartosc, sumaZaplacono, sumaSaldo;
            sumaWartosc = sumaZaplacono = sumaSaldo = 0;
            using (ExcelPackage excel = new ExcelPackage())
            {
                //excel.Workbook.Worksheets.Add(zakupSumyWorksheetName);
                excel.Workbook.Worksheets.Add(zakupSumy2WorksheetName);
                excel.Workbook.Worksheets.Add(zakupWorksheetName);
                var worksheet = excel.Workbook.Worksheets[zakupWorksheetName];
                //var worksheet2 = excel.Workbook.Worksheets[zakupSumyWorksheetName];
                var worksheet3 = excel.Workbook.Worksheets[zakupSumy2WorksheetName];
                Color sumRowColumnColor = ColorTranslator.FromHtml("#B7DEE8");

                string headerRange = "";
                int wiersz = 3;
                HeaderSzczegolowy(worksheet, sumRowColumnColor, ref headerRange);
                RowsSzczegolowy(ref wiersz, worksheet, ref sumaWartosc, ref sumaZaplacono, ref sumaSaldo, sumRowColumnColor, "WSZYSCY", stan_na_dzien);
                StyleSzczegolowy(headerRange, wiersz, worksheet);

                //string headerRange2 = "";
                //int wiersz2 = 3;
                //HeaderSumy(worksheet2, sumRowColumnColor, ref headerRange2);
                //RowsSumy(ref wiersz2, worksheet2, "WSZYSCY", sumRowColumnColor, stan_na_dzien);
                //StyleSumy(headerRange2, wiersz2, worksheet2);

                string headerRange3 = "";
                int wiersz3 = 3;
                HeaderSumy2(worksheet3, sumRowColumnColor, ref headerRange3);
                RowsSumy2(ref wiersz3, worksheet3, "WSZYSCY", sumRowColumnColor, stan_na_dzien);
                StyleSumy2(headerRange3, wiersz3, worksheet3);

                if (!testowy)
                {
                    excelFile = new FileInfo(path);
                }
                else
                {
                    excelFile = new FileInfo(testPath);
                }
                excel.SaveAs(excelFile); //MessageBox.Show("Plik stworzony");
            }
        } //Automatyczny raport

        private static void RaportSaldoKontrahentZakup(bool testowy, string nazwaKontrahenta)
        {
            double sumaWartosc, sumaZaplacono, sumaSaldo;
            sumaWartosc = sumaZaplacono = sumaSaldo = 0;

            Color sumRowColumnColor = ColorTranslator.FromHtml("#B7DEE8");
            string start, etap1, etap2, etap3, etap4, etap5, etap6, koniec;

            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Worksheets.Add(zakupSumy2WorksheetName);
                excel.Workbook.Worksheets.Add(zakupWorksheetName);
                //excel.Workbook.Worksheets.Add(zakupSumyWorksheetName);
                var worksheet = excel.Workbook.Worksheets[zakupWorksheetName];
                //var worksheet2 = excel.Workbook.Worksheets[zakupSumyWorksheetName];
                var worksheet3 = excel.Workbook.Worksheets[zakupSumy2WorksheetName];

                start = DateTime.Now.ToShortTimeString();

                string headerRange = "";
                int wiersz = 3;
                HeaderSzczegolowy(worksheet, sumRowColumnColor, ref headerRange);
                RowsSzczegolowy(ref wiersz, worksheet, ref sumaWartosc, ref sumaZaplacono, ref sumaSaldo, sumRowColumnColor, nazwaKontrahenta, stan_na_dzien);
                StyleSzczegolowy(headerRange, wiersz, worksheet);

                etap1 = DateTime.Now.ToShortTimeString();

                //string headerRange2 = "";
                //int wiersz2 = 3;
                //HeaderSumy(worksheet2, sumRowColumnColor, ref headerRange2);
                //RowsSumy(ref wiersz2, worksheet2, nazwaKontrahenta, sumRowColumnColor, stan_na_dzien);
                //StyleSumy(headerRange2, wiersz2, worksheet2);

                etap2 = DateTime.Now.ToShortTimeString();

                string headerRange3 = "";
                int wiersz3 = 3;
                HeaderSumy2(worksheet3, sumRowColumnColor, ref headerRange3);
                RowsSumy2(ref wiersz3, worksheet3, "WSZYSCY", sumRowColumnColor, stan_na_dzien);
                StyleSumy2(headerRange3, wiersz3, worksheet3);
                if (!testowy)
                {
                    excelFile = new FileInfo(path);
                }
                else
                {
                    excelFile = new FileInfo(testPath);
                }
                excel.SaveAs(excelFile); //MessageBox.Show("Plik stworzony");
            }

            using (ExcelPackage excel = new ExcelPackage())
            {

                excel.Workbook.Worksheets.Add(rozliczenieKontBankowychBiezacyMiesiacName);
                excel.Workbook.Worksheets.Add(rozliczenieKontBankowychOstatnieTrzyMiesiaceName);
                excel.Workbook.Worksheets.Add(rozliczenieKontBankowychOstatnieSześćMiesiacyName);
                excel.Workbook.Worksheets.Add(rozliczenieKontBankowychOstatniRokName);

                var worksheet4 = excel.Workbook.Worksheets[rozliczenieKontBankowychBiezacyMiesiacName];
                var worksheet5 = excel.Workbook.Worksheets[rozliczenieKontBankowychOstatnieTrzyMiesiaceName];
                var worksheet6 = excel.Workbook.Worksheets[rozliczenieKontBankowychOstatnieSześćMiesiacyName];
                var worksheet7 = excel.Workbook.Worksheets[rozliczenieKontBankowychOstatniRokName];



                etap3 = DateTime.Now.ToLongTimeString();

                string headerRange4 = "";
                int wiersz4 = 3;
                Header4(worksheet4, sumRowColumnColor, ref headerRange4);
                Rows4(ref wiersz4, worksheet4, "WSZYSCY", sumRowColumnColor, stan_na_dzien, 1);
                Style4(headerRange4, wiersz4, worksheet4);

                etap4 = DateTime.Now.ToLongTimeString();

                string headerRange5 = "";
                int wiersz5 = 2;
                Header4(worksheet5, sumRowColumnColor, ref headerRange5);
                Rows4(ref wiersz5, worksheet5, "WSZYSCY", sumRowColumnColor, stan_na_dzien, 2);
                Style4(headerRange5, wiersz5, worksheet5);

                etap5 = DateTime.Now.ToLongTimeString();

                string headerRange6 = "";
                int wiersz6 = 2;
                Header4(worksheet6, sumRowColumnColor, ref headerRange6);
                Rows4(ref wiersz6, worksheet6, "WSZYSCY", sumRowColumnColor, stan_na_dzien, 3);
                Style4(headerRange6, wiersz6, worksheet6);

                etap6 = DateTime.Now.ToLongTimeString();

                string headerRange7 = "";
                int wiersz7 = 2;
                Header4(worksheet7, sumRowColumnColor, ref headerRange7);
                Rows4(ref wiersz7, worksheet7, "WSZYSCY", sumRowColumnColor, stan_na_dzien, 4);
                Style4(headerRange7, wiersz7, worksheet7);

                koniec = DateTime.Now.ToShortTimeString();

                if (!testowy)
                {
                    excelFile2 = new FileInfo(path2);
                }
                else
                {
                    excelFile2 = new FileInfo(testPath2);
                }
                excel.SaveAs(excelFile2); //MessageBox.Show("Plik stworzony");
            }
            string czasy = string.Format("Etap1: {0}{1}Etap 2: {2}{1}Etap 3: {3}{1}Etap 4: {5}{1}Etap 5: {6}{1}Etap 6: {7}{1}Etap 7: {8}{1}Koniec: {4}", start, Environment.NewLine, etap1, etap2, koniec, etap3, etap4, etap5, etap6);
        } //Manualny raport - konkretny kontrahent

        private static void HeaderSzczegolowy(ExcelWorksheet worksheet, Color sumRowColumnColor, ref string headerRange)
        {
            var headerRow = new List<string[]>()
                {
                    new string[] { "Kontrahent (nazwa)", "NIP", "Faktura", "Wartość brutto", "Zapłacono", "Saldo", "Data płatności", "Po terminie", string.Format("Możliwość zmiany na http://int.poburski.local/aplikacjeASPX/05/005/RaportyKontrahenci.aspx") }
                };
            headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
            worksheet.Cells[headerRange].LoadFromArrays(headerRow);

            worksheet.Cells[headerRange].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[headerRange].Style.Fill.BackgroundColor.SetColor(sumRowColumnColor);
        }

        private static void HeaderSumy(ExcelWorksheet worksheet2, Color sumRowColumnColor, ref string headerRange2)
        {
            //sumy
            var headerRow2 = new List<string[]>()
                {
                    new string[] { "Kontrahent (nazwa)", "NIP", "Saldo", "Saldo przed terminem", "Saldo po terminie" }
                };
            headerRange2 = "A1:" + Char.ConvertFromUtf32(headerRow2[0].Length + 64) + "1";
            worksheet2.Cells[headerRange2].LoadFromArrays(headerRow2);

            worksheet2.Cells[headerRange2].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet2.Cells[headerRange2].Style.Fill.BackgroundColor.SetColor(sumRowColumnColor);
        }

        private static void HeaderSumy2(ExcelWorksheet worksheet3, Color sumRowColumnColor, ref string headerRange3)
        {
            //sumy
            var headerRow2 = new List<string[]>()
                {
                    new string[] { "Kontrahent (nazwa)", "NIP", "Stan na dzień", "Saldo", "Saldo po terminie", "1 - 7 dni", "8 - 14 dni", "15 - 30 dni", "31 - 60 dni", "61 + dni" }
                };
            headerRange3 = "A1:" + Char.ConvertFromUtf32(headerRow2[0].Length + 64) + "1";
            worksheet3.Cells[headerRange3].LoadFromArrays(headerRow2);

            worksheet3.Cells[headerRange3].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet3.Cells[headerRange3].Style.Fill.BackgroundColor.SetColor(sumRowColumnColor);
        }

        private static void Header4(ExcelWorksheet worksheet3, Color sumRowColumnColor, ref string headerRange3)
        {
            //sumy
            var headerRow2 = new List<string[]>()
                {
                    new string[] { "Bank", "Numer konta", "Data", "Kwota wpływów (z zatrzymaniami)" }
                };
            headerRange3 = "A1:" + Char.ConvertFromUtf32(headerRow2[0].Length + 64) + "1";
            worksheet3.Cells[headerRange3].LoadFromArrays(headerRow2);

            worksheet3.Cells[headerRange3].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet3.Cells[headerRange3].Style.Fill.BackgroundColor.SetColor(sumRowColumnColor);
        }

        private static void StyleSzczegolowy(string headerRange, int wiersz, ExcelWorksheet worksheet)
        {
            //A1:H1 - Header
            worksheet.Cells[headerRange].Style.Font.Bold = true;
            worksheet.Cells[headerRange].Style.Font.Size = 14;
            worksheet.Cells[headerRange].Style.Font.Color.SetColor(System.Drawing.Color.Black);

            //zmiana rozmiaru kolumny w zależności od zawartości - A1:H?
            worksheet.Cells.AutoFitColumns();

            //Borders - A1:H?
            var koniecWierszy = wiersz - 1;
            worksheet.Cells["A1:H" + koniecWierszy].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            worksheet.Cells["A1:H" + koniecWierszy].Style.Border.Top.Color.SetColor(Color.Black);
            worksheet.Cells["A1:H" + koniecWierszy].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            worksheet.Cells["A1:H" + koniecWierszy].Style.Border.Bottom.Color.SetColor(Color.Black);
            worksheet.Cells["A1:H" + koniecWierszy].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            worksheet.Cells["A1:H" + koniecWierszy].Style.Border.Right.Color.SetColor(Color.Black);
            worksheet.Cells["A1:H" + koniecWierszy].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            worksheet.Cells["A1:H" + koniecWierszy].Style.Border.Left.Color.SetColor(Color.Black);

            try
            {
                //liczby na prawo - C2:H?
                worksheet.Cells["C2:H" + koniecWierszy].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            }
            catch (Exception)
            {

            }

            //włączenie filtru na pierwszej kolumnie
            worksheet.Cells["A1:H" + koniecWierszy].AutoFilter = true;
        }

        private static void StyleSumy(string headerRange2, int wiersz2, ExcelWorksheet worksheet2)
        {
            //A1:H1 - Header
            worksheet2.Cells[headerRange2].Style.Font.Bold = true;
            worksheet2.Cells[headerRange2].Style.Font.Size = 14;
            worksheet2.Cells[headerRange2].Style.Font.Color.SetColor(System.Drawing.Color.Black);

            //zmiana rozmiaru kolumny w zależności od zawartości - A1:H?
            worksheet2.Cells.AutoFitColumns();

            //Borders - A1:H?
            var koniecWierszy2 = wiersz2 - 1;
            worksheet2.Cells["A1:E" + koniecWierszy2].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            worksheet2.Cells["A1:E" + koniecWierszy2].Style.Border.Top.Color.SetColor(Color.Black);
            worksheet2.Cells["A1:E" + koniecWierszy2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            worksheet2.Cells["A1:E" + koniecWierszy2].Style.Border.Bottom.Color.SetColor(Color.Black);
            worksheet2.Cells["A1:E" + koniecWierszy2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            worksheet2.Cells["A1:E" + koniecWierszy2].Style.Border.Right.Color.SetColor(Color.Black);
            worksheet2.Cells["A1:E" + koniecWierszy2].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            worksheet2.Cells["A1:E" + koniecWierszy2].Style.Border.Left.Color.SetColor(Color.Black);

            //liczby na prawo - C2:H?
            worksheet2.Cells["C2:E" + koniecWierszy2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

            //włączenie filtru na pierwszej kolumnie
            worksheet2.Cells["A1:E" + koniecWierszy2].AutoFilter = true;
        }

        private static void StyleSumy2(string headerRange2, int wiersz2, ExcelWorksheet worksheet2)
        {
            //A1:H1 - Header
            worksheet2.Cells[headerRange2].Style.Font.Bold = true;
            worksheet2.Cells[headerRange2].Style.Font.Size = 14;
            worksheet2.Cells[headerRange2].Style.Font.Color.SetColor(System.Drawing.Color.Black);

            //zmiana rozmiaru kolumny w zależności od zawartości - A1:H?
            worksheet2.Cells.AutoFitColumns();

            //Borders - A1:F?
            var koniecWierszy2 = wiersz2 - 1;
            worksheet2.Cells["A1:J" + koniecWierszy2].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            worksheet2.Cells["A1:J" + koniecWierszy2].Style.Border.Top.Color.SetColor(Color.Black);
            worksheet2.Cells["A1:J" + koniecWierszy2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            worksheet2.Cells["A1:J" + koniecWierszy2].Style.Border.Bottom.Color.SetColor(Color.Black);
            worksheet2.Cells["A1:J" + koniecWierszy2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            worksheet2.Cells["A1:J" + koniecWierszy2].Style.Border.Right.Color.SetColor(Color.Black);
            worksheet2.Cells["A1:J" + koniecWierszy2].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            worksheet2.Cells["A1:J" + koniecWierszy2].Style.Border.Left.Color.SetColor(Color.Black);

            //liczby na prawo - D2:F?
            worksheet2.Cells["D2:J" + koniecWierszy2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

            //Format 00.00 zł
            worksheet2.Cells["D2:J" + koniecWierszy2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

            //włączenie filtru na pierwszej kolumnie
            worksheet2.Cells["A1:J" + koniecWierszy2].AutoFilter = true;
        }

        private static void Style4(string headerRange2, int wiersz2, ExcelWorksheet worksheet2)
        {
            //A1:H1 - Header
            worksheet2.Cells[headerRange2].Style.Font.Bold = true;
            worksheet2.Cells[headerRange2].Style.Font.Size = 14;
            worksheet2.Cells[headerRange2].Style.Font.Color.SetColor(System.Drawing.Color.Black);

            //zmiana rozmiaru kolumny w zależności od zawartości - A1:H?
            worksheet2.Cells.AutoFitColumns();

            //Borders - A1:E?
            var koniecWierszy2 = wiersz2 - 1;
            worksheet2.Cells["A1:D" + koniecWierszy2].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            worksheet2.Cells["A1:D" + koniecWierszy2].Style.Border.Top.Color.SetColor(Color.Black);
            worksheet2.Cells["A1:D" + koniecWierszy2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            worksheet2.Cells["A1:D" + koniecWierszy2].Style.Border.Bottom.Color.SetColor(Color.Black);
            worksheet2.Cells["A1:D" + koniecWierszy2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            worksheet2.Cells["A1:D" + koniecWierszy2].Style.Border.Right.Color.SetColor(Color.Black);
            worksheet2.Cells["A1:D" + koniecWierszy2].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            worksheet2.Cells["A1:D" + koniecWierszy2].Style.Border.Left.Color.SetColor(Color.Black);

            //liczby na prawo - D2:E?
            worksheet2.Cells["D2:D" + koniecWierszy2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

            //Format 00.00 zł
            worksheet2.Cells["D2:D" + koniecWierszy2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

            //włączenie filtru na pierwszej kolumnie
            worksheet2.Cells["A1:D" + koniecWierszy2].AutoFilter = true;
        }

        private static void RowsSzczegolowy(ref int wiersz, ExcelWorksheet worksheet, ref double sumaWartosc, ref double sumaZaplacono, ref double sumaSaldo, Color sumRowColumnColor, string kontrahenciString, string stan_na_dzien)
        {
            DataClasses1DataContext db1 = new DataClasses1DataContext();
            db1.CommandTimeout = 1 * 60 * 30;
            DbManager db = new DbManager();
            List<int> listaKontrahentId = db.getKontrahenci(kontrahenciString, db1, true);

            wiersz = 2;
            foreach (int kontrahentId in listaKontrahentId)
            {
                try
                {
                    string nazwa = null;
                    string nip = null;
                    string faktura = null;
                    string data = null;
                    string wartosc = null;
                    string zaplacono = null;
                    string saldo = null;
                    string poTerminie = null;
                    bool czySumowac = false;

                    string stan_na_dzien2 = DateTime.Parse(stan_na_dzien).ToString("dd-MM-yyyy");
                    var g = from a in db1.dok_zak_salda_na_dzien('F', 'P', 'P', kontrahentId, "%", "%", stan_na_dzien2) orderby a.data_platnosci descending select a;
                    foreach (var row in g)
                    {
                        bool status = db.GetZakupData(ref nazwa, ref nip, ref faktura, ref data, ref wartosc, ref zaplacono, ref saldo, ref poTerminie, kontrahentId, row, stan_na_dzien, false);
                        //MessageBox.Show("Czyta pojedyncze");
                        if (status)
                        {
                            czySumowac = true;
                            double mWartosc = Math.Round(double.Parse(wartosc), 2);
                            double mZaplacono = Math.Round(double.Parse(zaplacono), 2);
                            double mSaldo = Math.Round(double.Parse(saldo), 2);
                            var Row = new List<object[]>()
                                {
                                    new object[] { nazwa, nip, faktura, mWartosc, mZaplacono, mSaldo, data, poTerminie }
                                };
                            string rowRange = "A" + wiersz + ":" + Char.ConvertFromUtf32(Row[0].Length + 64) + wiersz;
                            string sumCurrencyFormattedRowRange = "D2:" + Char.ConvertFromUtf32(Row[0].Length + 64) + wiersz;
                            worksheet.Cells[rowRange].LoadFromArrays(Row);
                            worksheet.Cells[sumCurrencyFormattedRowRange].Style.Numberformat.Format = "#,##0.00 zł";
                            wiersz++;

                            sumaWartosc += Convert.ToDouble(wartosc);
                            sumaZaplacono += Convert.ToDouble(zaplacono);
                            sumaSaldo += Convert.ToDouble(saldo);
                        }
                    }
                    if (czySumowac)
                    {
                        var SumRow = new List<object[]>()
                                {
                                    new object[] { nazwa, nip, "SUMA:", Math.Round(sumaWartosc,2), Math.Round(sumaZaplacono,2), Math.Round(sumaSaldo,2), "", "" }
                                };
                        string sumRowRange = "A" + wiersz + ":" + Char.ConvertFromUtf32(SumRow[0].Length + 64) + wiersz;
                        string sumCurrencyFormattedRowRange = "D2:" + Char.ConvertFromUtf32(SumRow[0].Length + 64) + wiersz;
                        worksheet.Cells[sumRowRange].LoadFromArrays(SumRow);

                        worksheet.Cells[sumRowRange].Style.Font.Bold = true;
                        worksheet.Cells[sumRowRange].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[sumRowRange].Style.Fill.BackgroundColor.SetColor(sumRowColumnColor);
                        worksheet.Cells[sumCurrencyFormattedRowRange].Style.Numberformat.Format = "#,##0.00 zł";



                        sumaWartosc = sumaZaplacono = sumaSaldo = 0;
                        czySumowac = false;
                        wiersz++;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private static void RowsSumy(ref int wiersz2, ExcelWorksheet worksheet2, string nazwaKontrahenta, Color sumRowColumnColor, string stan_na_dzien)
        {
            DataClasses1DataContext db1 = new DataClasses1DataContext();
            db1.CommandTimeout = 1 * 60 * 30;
            DbManager db = new DbManager();
            List<int> listaKontrahentId = db.getKontrahenci(nazwaKontrahenta, db1, false);
            double saldoSuma = 0, saldoPo = 0, saldoPrzed = 0;

            double saldoPodsumowanie = 0, saldoPoPodsumowanie = 0, saldoPrzedPodsumowanie = 0;

            wiersz2 = 2;
            foreach (int kontrahentId in listaKontrahentId)
            {
                try
                {
                    string nazwa = null;
                    string nip = null;
                    string faktura = null;
                    string data = null;
                    string wartosc = null;
                    string zaplacono = null;
                    string saldo = null;
                    string poTerminie = null;
                    bool wypisz = false;

                    string stan_na_dzien2 = DateTime.Parse(stan_na_dzien).ToString("dd-MM-yyyy");
                    var g = from a in db1.dok_zak_salda_na_dzien('F', 'P', 'P', kontrahentId, "%", "%", stan_na_dzien2) orderby a.data_platnosci descending select a;

                    foreach (var row in g)
                    {
                        bool status = db.GetZakupData(ref nazwa, ref nip, ref faktura, ref data, ref wartosc, ref zaplacono, ref saldo, ref poTerminie, kontrahentId, row, stan_na_dzien, true);
                        double sumaWartosc = Convert.ToDouble(saldo);
                        saldoSuma += sumaWartosc;
                        //MessageBox.Show("Czyta pojedyncze");
                        if (status)
                        {
                            wypisz = true;
                            saldoPodsumowanie += sumaWartosc;
                            if (!string.IsNullOrEmpty(poTerminie))
                            {
                                saldoPo += sumaWartosc;
                                saldoPoPodsumowanie += sumaWartosc;
                            }
                            else
                            {
                                saldoPrzed += sumaWartosc;
                                saldoPrzedPodsumowanie += sumaWartosc;
                            }
                        }
                    }
                    if (wypisz)
                    {
                        var SumRow = new List<object[]>()
                                {
                                    new object[] { nazwa, nip, Math.Round(saldoSuma,2), Math.Round(saldoPrzed,2), Math.Round(saldoPo,2) }
                                };
                        string sumRowRange = "A" + wiersz2 + ":" + Char.ConvertFromUtf32(SumRow[0].Length + 64) + wiersz2;
                        string sumCurrencyFormattedRowRange = "C2:" + Char.ConvertFromUtf32(SumRow[0].Length + 64) + wiersz2;
                        worksheet2.Cells[sumRowRange].LoadFromArrays(SumRow);

                        worksheet2.Cells[sumCurrencyFormattedRowRange].Style.Numberformat.Format = "#,##0.00 zł";

                        saldoSuma = saldoPrzed = saldoPo = 0;
                        wypisz = false;

                        wiersz2++;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            var PodsumowanieRow = new List<object[]>()
                                {
                                    new object[] { "SUMA", "", Math.Round(saldoPodsumowanie,2), Math.Round(saldoPrzedPodsumowanie,2), Math.Round(saldoPoPodsumowanie,2) }
                                };
            string sumPodsumowanieRowRange = "A" + wiersz2 + ":" + Char.ConvertFromUtf32(PodsumowanieRow[0].Length + 64) + wiersz2;
            string sumPodsumowanieCurrencyFormattedRowRange = "C2:" + Char.ConvertFromUtf32(PodsumowanieRow[0].Length + 64) + wiersz2;
            worksheet2.Cells[sumPodsumowanieRowRange].LoadFromArrays(PodsumowanieRow);

            worksheet2.Cells[sumPodsumowanieRowRange].Style.Font.Bold = true;
            worksheet2.Cells[sumPodsumowanieRowRange].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet2.Cells[sumPodsumowanieRowRange].Style.Fill.BackgroundColor.SetColor(sumRowColumnColor);
            worksheet2.Cells[sumPodsumowanieCurrencyFormattedRowRange].Style.Numberformat.Format = "#,##0.00 zł";

            wiersz2++;
        }

        private static void RowsSumy2(ref int wiersz2, ExcelWorksheet worksheet2, string nazwaKontrahenta, Color sumRowColumnColor, string stan_na_dzien)
        {
            DataClasses1DataContext db1 = new DataClasses1DataContext();
            db1.CommandTimeout = 1 * 60 * 30;
            DbManager db = new DbManager();
            List<int> listaKontrahentId = db.getKontrahenci(nazwaKontrahenta, db1, false);
            double saldoSuma = 0, saldoPo = 0, saldoPrzed = 0;

            double saldoPodsumowanie = 0, saldoPoPodsumowanie = 0;

            wiersz2 = 2;
            double sumaOd1do7dni = 0;
            double sumaOd8do14dni = 0;
            double sumaOd15do30dni = 0;
            double sumaOd31do60dni = 0;
            double sumaOd61doNdni = 0;
            foreach (int kontrahentId in listaKontrahentId)
            {
                try
                {
                    string nazwa = null;
                    string nip = null;
                    string faktura = null;
                    string data = null;
                    string wartosc = null;
                    string zaplacono = null;
                    string saldo = null;
                    string poTerminie = null;
                    double od1do7dni = 0;
                    double od8do14dni = 0;
                    double od15do30dni = 0;
                    double od31do60dni = 0;
                    double od61doNdni = 0;
                    saldoSuma = 0;

                    bool wypisz = false;

                    try
                    {
                        stan_na_dzien = DateTime.Parse(stan_na_dzien).ToString("MM-dd-yyyy");
                    }
                    catch (Exception e1)
                    {
                        Console.WriteLine(e1);
                        try
                        {
                            string[] lista = stan_na_dzien.Split('-');
                            string day = lista[0];
                            string month = lista[1];
                            string year = lista[2];

                            stan_na_dzien = string.Format("{0}-{1}-{2}", month, day, year);
                        }
                        catch (Exception e2)
                        {
                            Console.WriteLine(e2);
                        }
                    }

                    string stan_na_dzien2 = DateTime.Parse(stan_na_dzien).ToString("dd-MM-yyyy");
                    var g = from a in db1.dok_zak_salda_na_dzien('F', 'P', 'P', kontrahentId, "%", "%", stan_na_dzien2) orderby a.data_platnosci descending select a;

                    foreach (var row in g)
                    {
                        bool status = db.GetZakupData(ref nazwa, ref nip, ref faktura, ref data, ref wartosc, ref zaplacono, ref saldo, ref poTerminie, kontrahentId, row, stan_na_dzien, false);
                        //MessageBox.Show("Czyta pojedyncze");
                        double sumaWartosc = Convert.ToDouble(saldo);
                        saldoSuma += sumaWartosc;
                        saldoPodsumowanie += sumaWartosc;
                        //if (status)
                        //{
                        wypisz = true;
                        if (!string.IsNullOrEmpty(poTerminie))
                        {
                            saldoPo += sumaWartosc;
                            saldoPoPodsumowanie += sumaWartosc;
                        }
                        try
                        {
                            var poTerminieLiczba = Convert.ToInt32(poTerminie.Split(' ')[0]);
                            if (poTerminieLiczba >= 1 && poTerminieLiczba < 8)
                            {
                                od1do7dni += sumaWartosc;
                                sumaOd1do7dni += sumaWartosc;
                            }
                            if (poTerminieLiczba > 7 && poTerminieLiczba < 15)
                            {
                                od8do14dni += sumaWartosc;
                                sumaOd8do14dni += sumaWartosc;
                            }
                            if (poTerminieLiczba > 14 && poTerminieLiczba < 31)
                            {
                                od15do30dni += sumaWartosc;
                                sumaOd15do30dni += sumaWartosc;
                            }
                            if (poTerminieLiczba > 30 && poTerminieLiczba < 61)
                            {
                                od31do60dni += sumaWartosc;
                                sumaOd31do60dni += sumaWartosc;
                            }
                            if (poTerminieLiczba > 60)
                            {
                                od61doNdni += sumaWartosc;
                                sumaOd61doNdni += sumaWartosc;
                            }
                        }
                        catch (Exception ex5)
                        {
                            Debug.WriteLine(row.idkontahenta + " " + nazwa + Environment.NewLine + ex5.Message);
                            od1do7dni += 0;
                            sumaOd1do7dni += 0;
                            od8do14dni += 0;
                            sumaOd8do14dni += 0;
                            od15do30dni += 0;
                            sumaOd15do30dni += 0;
                            od31do60dni += 0;
                            sumaOd31do60dni += 0;
                            od61doNdni += 0;
                            sumaOd61doNdni += 0;
                        }
                        //}
                    }
                    if (wypisz)
                    {
                        var SumRow = new List<object[]>()
                                {
                                    new object[] { nazwa, nip, stan_na_dzien, Math.Round(saldoSuma,2), Math.Round(saldoPo,2), Math.Round(od1do7dni, 2), Math.Round(od8do14dni, 2), Math.Round(od15do30dni, 2), Math.Round(od31do60dni, 2), Math.Round(od61doNdni, 2) }
                                };
                        string sumRowRange = "A" + wiersz2 + ":" + Char.ConvertFromUtf32(SumRow[0].Length + 64) + wiersz2;
                        string sumCurrencyFormattedRowRange = "D2:" + Char.ConvertFromUtf32(SumRow[0].Length + 64) + wiersz2;
                        worksheet2.Cells[sumRowRange].LoadFromArrays(SumRow);

                        worksheet2.Cells[sumCurrencyFormattedRowRange].Style.Numberformat.Format = "#,##0.00 zł";

                        saldoSuma = saldoPrzed = saldoPo = 0;
                        wypisz = false;

                        wiersz2++;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            var PodsumowanieRow = new List<object[]>()
                                {
                                    new object[] { "SUMA", "", "", Math.Round(saldoPodsumowanie,2), Math.Round(saldoPoPodsumowanie,2), Math.Round(sumaOd1do7dni, 2), Math.Round(sumaOd8do14dni, 2), Math.Round(sumaOd15do30dni, 2), Math.Round(sumaOd31do60dni, 2), Math.Round(sumaOd61doNdni, 2) }
                                };
            string sumPodsumowanieRowRange = "A" + wiersz2 + ":" + Char.ConvertFromUtf32(PodsumowanieRow[0].Length + 64) + wiersz2;
            string sumPodsumowanieCurrencyFormattedRowRange = "D2:" + Char.ConvertFromUtf32(PodsumowanieRow[0].Length + 64) + wiersz2;
            worksheet2.Cells[sumPodsumowanieRowRange].LoadFromArrays(PodsumowanieRow);

            worksheet2.Cells["D" + wiersz2].Formula = string.Format("SUM(D2:D{0})", wiersz2 - 1);

            worksheet2.Cells[sumPodsumowanieRowRange].Style.Font.Bold = true;
            worksheet2.Cells[sumPodsumowanieRowRange].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet2.Cells[sumPodsumowanieRowRange].Style.Fill.BackgroundColor.SetColor(sumRowColumnColor);
            worksheet2.Cells[sumPodsumowanieCurrencyFormattedRowRange].Style.Numberformat.Format = "#,##0.00 zł";

            wiersz2++;
        }

        private static void Rows4(ref int wiersz2, ExcelWorksheet worksheet2, string nazwaKontrahenta, Color sumRowColumnColor, string stan_na_dzien, int ktory)
        {
            DataClasses1DataContext db1 = new DataClasses1DataContext();
            db1.CommandTimeout = 1 * 60 * 30;
            List<selectRozliczeniaKontBankowychResult> lista = new List<selectRozliczeniaKontBankowychResult>();
            var last = Enumerable.Range(1, 6).Select(i => DateTime.Now.AddMonths(-i)).ToList();
            switch (ktory)
            {
                case 2:
                    lista = db1.selectRozliczeniaKontBankowych(2, DateTime.Now.Year, DateTime.Now.Month).ToList();
                    last = Enumerable.Range(1, 3).Select(i => DateTime.Now.AddMonths(-i)).ToList();
                    break;
                case 3:
                    lista = db1.selectRozliczeniaKontBankowych(3, DateTime.Now.Year, DateTime.Now.Month).ToList();
                    break;
                case 4:
                    lista = db1.selectRozliczeniaKontBankowych(4, DateTime.Now.Year, DateTime.Now.Month).ToList();
                    last = Enumerable.Range(1, 12).Select(i => DateTime.Now.AddMonths(-i)).ToList();
                    break;
                default:
                    lista = db1.selectRozliczeniaKontBankowych(1, DateTime.Now.Year, DateTime.Now.Month).ToList();
                    last = Enumerable.Range(0, 1).Select(i => DateTime.Now.AddMonths(-i)).ToList();
                    break;
            }
            #region Wersja 1
            //wiersz2 = 2;
            //var poprzednik = lista[0];
            //foreach (var rekord in lista)
            //{
            //    var SumRow = new List<object[]>()
            //                    {
            //                        new object[] { rekord.bank, rekord.Nrkonta, DataSlownie(rekord.miesiac, rekord.rok), rekord.suma }
            //                    };
            //    var SumRowEmpty = new List<object[]>()
            //                    {
            //                        new object[] { "", "", "", "" }
            //                    };
            //    string srednia = Convert.ToDouble(lista.Where(l => l.bank == poprzednik.bank).Sum(l => l.suma)).ToString("C");
            //    switch (ktory)
            //    {
            //        case 2:
            //            srednia = Convert.ToDouble(lista.Where(l => l.bank == poprzednik.bank).Sum(l => l.suma) / 3).ToString("C");
            //            break;
            //        case 3:
            //            srednia = Convert.ToDouble(lista.Where(l => l.bank == poprzednik.bank).Sum(l => l.suma) / 6).ToString("C");
            //            break;
            //        case 4:
            //            srednia = Convert.ToDouble(lista.Where(l => l.bank == poprzednik.bank).Sum(l => l.suma) / 12).ToString("C");
            //            break;
            //        default:
            //            break;
            //    }
            //    var SumRowPodsumowanie = new List<object[]>()
            //                    {
            //                        new object[] { string.Format("Wymóg średniomiesięczny: {0}", poprzednik.wymog.ToString("C")), string.Format("Średnia: {0}", srednia), "-", string.Format("Suma: {0}", Convert.ToDouble(lista.Where(l => l.bank == poprzednik.bank).Sum(l => l.suma)).ToString("C")) }
            //                    };
            //    string sumRowRange = "A" + wiersz2 + ":" + Char.ConvertFromUtf32(SumRow[0].Length + 64) + wiersz2;
            //    string redRowRange = "D" + wiersz2;
            //    string sumCurrencyFormattedRowRange = "D2:" + Char.ConvertFromUtf32(SumRow[0].Length + 64) + wiersz2;


            //    if (rekord != lista[0] && poprzednik.bank != rekord.bank)
            //    {
            //        //Podsumowanie
            //        worksheet2.Cells[sumRowRange].LoadFromArrays(SumRowPodsumowanie);
            //        worksheet2.Cells[sumRowRange].Style.Fill.PatternType = ExcelFillStyle.Solid;
            //        worksheet2.Cells[sumRowRange].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
            //        worksheet2.Cells[sumRowRange].Style.Font.Bold = true;

            //        if (poprzednik.wymog > Convert.ToDouble(srednia.Replace(" ", "").Replace("zł", "")))
            //        {
            //            string sredniaRowRange = "B" + wiersz2;
            //            worksheet2.Cells[sredniaRowRange].Style.Font.Color.SetColor(Color.Red);
            //        }

            //        wiersz2++;
            //        sumRowRange = "A" + wiersz2 + ":" + Char.ConvertFromUtf32(SumRow[0].Length + 64) + wiersz2;


            //    }
            //    if (poprzednik.bank == rekord.bank && poprzednik.Nrkonta != rekord.Nrkonta)
            //    {
            //        //Jeśli ten sam bank - kontynuuj
            //        worksheet2.Cells[sumRowRange].LoadFromArrays(SumRowEmpty);
            //        wiersz2++;
            //        sumRowRange = "A" + wiersz2 + ":" + Char.ConvertFromUtf32(SumRow[0].Length + 64) + wiersz2;
            //    }
            //    try
            //    {
            //        worksheet2.Cells[sumRowRange].LoadFromArrays(SumRow);

            //        worksheet2.Cells[sumCurrencyFormattedRowRange].Style.Numberformat.Format = "#,##0.00 zł";
            //        if (rekord.przekroczono == "NIE")
            //        {
            //            worksheet2.Cells[redRowRange].Style.Font.Color.SetColor(Color.Red);
            //        }

            //        wiersz2++;
            //    }
            //    catch (Exception ex)
            //    {
            //        Console.WriteLine(ex.Message);
            //    }

            //    if (rekord == lista.Last())
            //    {
            //        sumRowRange = "A" + wiersz2 + ":" + Char.ConvertFromUtf32(SumRow[0].Length + 64) + wiersz2;
            //        //Podsumowanie
            //        sumRowRange = "A" + wiersz2 + ":" + Char.ConvertFromUtf32(SumRow[0].Length + 64) + wiersz2;
            //        worksheet2.Cells[sumRowRange].Style.Fill.PatternType = ExcelFillStyle.Solid;
            //        worksheet2.Cells[sumRowRange].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
            //        srednia = Convert.ToDouble(lista.Where(l => l.bank == rekord.bank).Sum(l => l.suma)).ToString("C");
            //        SumRowPodsumowanie = new List<object[]>()
            //                    {
            //                        new object[] { string.Format("Wymóg średniomiesięczny: {0}", rekord.wymog.ToString("C")), string.Format("Średnia: {0}", srednia), "-", string.Format("Suma: {0}", Convert.ToDouble(lista.Where(l => l.bank == poprzednik.bank).Sum(l => l.suma)).ToString("C")) }
            //                    };
            //        worksheet2.Cells[sumRowRange].LoadFromArrays(SumRowPodsumowanie);
            //        worksheet2.Cells[sumRowRange].Style.Font.Bold = true;
            //        if (rekord.wymog > Convert.ToDouble(srednia.Replace(" ", "").Replace("zł", "")))
            //        {
            //            string sredniaRowRange = "B" + wiersz2;
            //            worksheet2.Cells[sredniaRowRange].Style.Font.Color.SetColor(Color.Red);
            //        }
            //    }

            //    poprzednik = rekord;
            //}
            //wiersz2++;
            #endregion

            try
            {
                var listaDistinct = lista.Select(d => d.Nrkonta).Where(d => d != "95175000090000000000884189" && d != "04236000050000004550116034").Distinct().ToList();
                var poprzednik = lista[0];
                var ostatni = lista.Last();
                var ostatnieKonto = new selectRozliczeniaKontBankowychResult();
                foreach (var rekord in listaDistinct)
                {
                    var rekordy = lista.Where(d => d.Nrkonta == rekord).ToList();
                    var listaKonta = new List<selectRozliczeniaKontBankowychResult>();
                    for (int i = 0; i < last.Count; i++)
                    {
                        bool koniec = false;
                        if (rekordy.Where(r => r.rok == last[i].Year && r.miesiac == last[i].Month).Any())
                        {
                            var konto = rekordy.Where(r => r.rok == last[i].Year && r.miesiac == last[i].Month).First();
                            listaKonta.Add(konto);
                            if (konto == lista.Last())
                            {
                                koniec = true;
                            }
                        }
                        else
                        {
                            selectRozliczeniaKontBankowychResult pusteKonto = new selectRozliczeniaKontBankowychResult();
                            pusteKonto.miesiac = last[i].Month;
                            pusteKonto.rok = last[i].Year;
                            pusteKonto.bank = rekordy[0].bank;
                            pusteKonto.Nrkonta = rekordy[0].Nrkonta;
                            pusteKonto.przekroczono = rekordy[0].przekroczono;
                            pusteKonto.kwota = 0.00;
                            pusteKonto.suma = 0.00;
                            pusteKonto.zatrz_bank = 0.00;
                            pusteKonto.zatrz_czas = 0.00;
                            pusteKonto.wymog = rekordy[0].wymog;
                            listaKonta.Add(pusteKonto);
                        }
                        if (koniec)
                        {
                            ostatni = listaKonta.OrderByDescending(o => o.rok).ThenByDescending(o => o.miesiac).Last();
                        }
                    }
                    foreach (var pole in listaKonta)
                    {
                        var SumRow = new List<object[]>()
                                {
                                    new object[] { pole.bank, pole.Nrkonta, DataSlownie(pole.miesiac, pole.rok), pole.suma }
                                };
                        var SumRowEmpty = new List<object[]>()
                                        {
                                            new object[] { "", "", "", "" }
                                        };
                        string sumRowRange = "A" + wiersz2 + ":" + Char.ConvertFromUtf32(SumRow[0].Length + 64) + wiersz2;
                        string redRowRange = "D" + wiersz2;

                        if (pole != lista[0] && poprzednik.bank != pole.bank)
                        {
                            //Podsumowanie
                            string srednia = Convert.ToDouble(lista.Where(l => l.bank == poprzednik.bank).Sum(l => l.suma)).ToString("C");
                            switch (ktory)
                            {
                                case 2:
                                    srednia = Convert.ToDouble(lista.Where(l => l.bank == poprzednik.bank).Sum(l => l.suma) / 3).ToString("C");
                                    break;
                                case 3:
                                    srednia = Convert.ToDouble(lista.Where(l => l.bank == poprzednik.bank).Sum(l => l.suma) / 6).ToString("C");
                                    break;
                                case 4:
                                    srednia = Convert.ToDouble(lista.Where(l => l.bank == poprzednik.bank).Sum(l => l.suma) / 12).ToString("C");
                                    break;
                                default:
                                    break;
                            }
                            var SumRowPodsumowanie = new List<object[]>()
                                        {
                                            new object[] { string.Format("Wymóg średniomiesięczny: {0}", poprzednik.wymog.ToString("C")), string.Format("Średnia: {0}", srednia), "-", string.Format("Suma: {0}", Convert.ToDouble(lista.Where(l => l.bank == poprzednik.bank).Sum(l => l.suma)).ToString("C")) }
                                        };

                            worksheet2.Cells[sumRowRange].LoadFromArrays(SumRowPodsumowanie);
                            worksheet2.Cells[sumRowRange].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet2.Cells[sumRowRange].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            worksheet2.Cells[sumRowRange].Style.Font.Bold = true;

                            if (poprzednik.wymog > Convert.ToDouble(srednia.Replace(" ", "").Replace("zł", "")) && poprzednik.wymog != 0)
                            {
                                string sredniaRowRange = "B" + wiersz2;
                                worksheet2.Cells[sredniaRowRange].Style.Font.Color.SetColor(Color.Red);
                            }

                            wiersz2++;
                            sumRowRange = "A" + wiersz2 + ":" + Char.ConvertFromUtf32(SumRow[0].Length + 64) + wiersz2;
                            redRowRange = "D" + wiersz2;

                        }
                        if (poprzednik.bank == pole.bank && poprzednik.Nrkonta != pole.Nrkonta)
                        {
                            //Jeśli ten sam bank - kontynuuj
                            worksheet2.Cells[sumRowRange].LoadFromArrays(SumRowEmpty);
                            wiersz2++;
                            sumRowRange = "A" + wiersz2 + ":" + Char.ConvertFromUtf32(SumRow[0].Length + 64) + wiersz2;
                        }
                        string sumCurrencyFormattedRowRange = "D2:" + Char.ConvertFromUtf32(SumRow[0].Length + 64) + wiersz2;
                        try
                        {
                            worksheet2.Cells[sumRowRange].LoadFromArrays(SumRow);

                            worksheet2.Cells[sumCurrencyFormattedRowRange].Style.Numberformat.Format = "#,##0.00 zł";
                            if (pole.przekroczono == "NIE")
                            {
                                worksheet2.Cells[redRowRange].Style.Font.Color.SetColor(Color.Red);
                            }

                            wiersz2++;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        poprzednik = pole;
                    }
                }

                //ostatni
                if (poprzednik.miesiac == last.Last().Month && poprzednik.rok == last.Last().Year)
                {
                    //Podsumowanie
                    var srednia2 = Convert.ToDouble(lista.Where(l => l.bank == ostatni.bank).Sum(l => l.suma)).ToString("C");
                    switch (ktory)
                    {
                        case 2:
                            srednia2 = Convert.ToDouble(lista.Where(l => l.bank == ostatni.bank).Sum(l => l.suma) / 3).ToString("C");
                            break;
                        case 3:
                            srednia2 = Convert.ToDouble(lista.Where(l => l.bank == ostatni.bank).Sum(l => l.suma) / 6).ToString("C");
                            break;
                        case 4:
                            srednia2 = Convert.ToDouble(lista.Where(l => l.bank == ostatni.bank).Sum(l => l.suma) / 12).ToString("C");
                            break;
                        default:
                            break;
                    }


                    var SumRowPodsumowanie2 = new List<object[]>()
                                        {
                                            new object[] { string.Format("Wymóg średniomiesięczny: {0}", ostatni.wymog.ToString("C")), string.Format("Średnia: {0}", srednia2), "-", string.Format("Suma: {0}", Convert.ToDouble(lista.Where(l => l.bank == ostatni.bank).Sum(l => l.suma)).ToString("C")) }
                                        };
                    var lastRowRange = "A" + wiersz2 + ":" + Char.ConvertFromUtf32(SumRowPodsumowanie2[0].Length + 64) + wiersz2;
                    worksheet2.Cells[lastRowRange].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet2.Cells[lastRowRange].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    worksheet2.Cells[lastRowRange].LoadFromArrays(SumRowPodsumowanie2);
                    worksheet2.Cells[lastRowRange].Style.Font.Bold = true;
                    if (ostatni.wymog > Convert.ToDouble(srednia2.Replace(" ", "").Replace("zł", "")) && ostatni.wymog != 0)
                    {
                        string sredniaRowRange = "B" + wiersz2;
                        worksheet2.Cells[sredniaRowRange].Style.Font.Color.SetColor(Color.Red);
                    }
                    wiersz2++;
                }
            }
            catch (Exception e)
            {
                var SumRowPodsumowanie2 = new List<object[]>()
                                        {
                                            new object[] { "BRAK OPERACJI" }
                                        };
                var lastRowRange = "A" + wiersz2 + ":" + Char.ConvertFromUtf32(SumRowPodsumowanie2[0].Length + 64) + wiersz2;
                worksheet2.Cells[lastRowRange].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet2.Cells[lastRowRange].Style.Fill.BackgroundColor.SetColor(Color.Red);
                worksheet2.Cells[lastRowRange].LoadFromArrays(SumRowPodsumowanie2);
                worksheet2.Cells[lastRowRange].Style.Font.Bold = true;
                wiersz2++;
            }
        }

        private void WypelnijComboKontrahent()
        {
            comboBox1.Items.Add("WSZYSCY (zgodnie z iNTRANET)");
            DataClasses1DataContext db1 = new DataClasses1DataContext();
            db1.CommandTimeout = 1 * 60 * 30;
            var kontrahent = (from kon2 in db1.kontrahents where kon2.nr_kontrahenta != null && kon2.nazwa != null && kon2.nazwa != "" orderby kon2.nazwa select kon2).ToList();
            foreach (var kon in kontrahent)
            {
                comboBox1.Items.Add(kon.nazwa);
            }
            comboBox1.SelectedIndex = 0;
        }

        //public static string WydzielLiczbeSpacjami(string input)
        //{
        //    decimal decimalMoneyValue = Convert.ToDecimal(input);
        //    return String.Format("{0:C}", decimalMoneyValue);

        //    input = Zamien(input);
        //    StringBuilder buffer = new StringBuilder();

        //    if (input.Contains(","))
        //    {
        //        var bufferElements = input.Split(',').ToList();
        //        buffer = new StringBuilder(bufferElements[1].Length * 3 / 2);
        //        for (int i = 0; i < bufferElements[1].Length; i++)
        //        {
        //            if ((i > 0) & (i % 3 == 0))
        //            {
        //                buffer.Append(" ");
        //            }
        //            buffer.Append(bufferElements[1][i]);
        //        }
        //        var result = (Zamien(buffer.ToString()) + "," + bufferElements[0]).Replace("- ","-");
        //        return result;
        //    }
        //    else
        //    {
        //        new StringBuilder(input.Length * 3 / 2);
        //        for (int i = 0; i < input.Length; i++)
        //        {
        //            if ((i > 0) & (i % 3 == 0))
        //                buffer.Append(" ");
        //            buffer.Append(input[i]);
        //        }
        //        var result = (Zamien(buffer.ToString())).Replace("- ", "-"); ;
        //        return result;
        //    }

        //}

        private static string DataSlownie(int? miesiac, int? rok)
        {
            switch (miesiac)
            {
                case 1:
                    return string.Format("Styczeń {0}", rok);
                case 2:
                    return string.Format("Luty {0}", rok);
                case 3:
                    return string.Format("Marzec {0}", rok);
                case 4:
                    return string.Format("Kwiecień {0}", rok);
                case 5:
                    return string.Format("Maj {0}", rok);
                case 6:
                    return string.Format("Czerwiec {0}", rok);
                case 7:
                    return string.Format("Lipiec {0}", rok);
                case 8:
                    return string.Format("Sierpień {0}", rok);
                case 9:
                    return string.Format("Wrzesień {0}", rok);
                case 10:
                    return string.Format("Październik {0}", rok);
                case 11:
                    return string.Format("Listopad {0}", rok);
                case 12:
                    return string.Format("Grudzień {0}", rok);
                default:
                    return "";
            }
        }

        public static string Zamien(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
}
